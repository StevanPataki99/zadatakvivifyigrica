package com.company.test;

public class Oruzije {

    private int damage;
    private boolean baceno = true;
    private TipOruzija tip;

    public Oruzije(TipOruzija tip) {
        this.tip = tip;

        switch (this.tip) {
            case MAC -> this.damage = 10;
            case KOPLJE -> this.damage = 15;
            case CAROLIJA -> this.damage = 20;
        }

    }

    public int getDamage() {
        return this.damage;
    }

    public void setDamage(int noviDamage) {
        this.damage = noviDamage;
    }

    public boolean getBaceno() {
        return this.baceno;
    }

    public void setBaceno(boolean baceno) {
        this.baceno = baceno;
    }

    public TipOruzija getTip() {
        return this.tip;
    }

    public void setTip(TipOruzija noviTip) {
        this.tip = noviTip;
    }

}
