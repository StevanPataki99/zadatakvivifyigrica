package com.company.test;

public class Macevalac extends Heroj {

    public Macevalac(Ranac ranac) {
        super(ranac);
        this.health = 100;
    }

    @Override
    public void napadni(Cudoviste napadnutoCudoviste) {
        int healthMete = napadnutoCudoviste.getHealth();
        healthMete = healthMete - this.trenutnoOruzije.getDamage();
        napadnutoCudoviste.setHealth(healthMete);
        Logger.getInstance().log("[Macevalac] je napao [" + napadnutoCudoviste + "] pomocu [" + this.trenutnoOruzije + "]");
    }

    @Override
    public void napadni() {

    }

    @Override
    public void uzmiOruzije(Oruzije oruzije) {
        try {
            switch (oruzije.getTip()) {
                case KOPLJE -> {
                    ranac.dodajOruzije(oruzije);
                    Logger.getInstance().log("[Macevalac] je pokupio oruzije [" + oruzije + "]");
                }
                case MAC -> {
                    ranac.dodajOruzije(oruzije);
                    Logger.getInstance().log("[Macevalac] je pokupio oruzije [" + oruzije + "]");
                }
                case CAROLIJA -> throw new Exception("Macevalac ne moze da koristi carolije!");
            }
        } catch (Exception e) {
            System.out.println("Exception: " + e);
        }
    }

}
