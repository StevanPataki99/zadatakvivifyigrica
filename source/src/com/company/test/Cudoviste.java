package com.company.test;

public abstract class Cudoviste implements Bice {

    protected int health;

    protected Cudoviste() {

    }

    public abstract void napadni(Heroj napadnutiHeroj);

    public int getHealth() {
        return this.health;
    }

    public void setHealth(int noviHealth) {
        this.health = noviHealth;
    }

}
