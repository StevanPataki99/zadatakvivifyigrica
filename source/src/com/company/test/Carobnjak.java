package com.company.test;

public class Carobnjak extends Heroj {

    public Carobnjak(Ranac ranac) {
        super(ranac);
        this.health = 150;
    }

    @Override
    public void napadni(Cudoviste napadnutoCudoviste) {
        int healthMete = napadnutoCudoviste.getHealth();
        healthMete = healthMete - this.trenutnoOruzije.getDamage();
        napadnutoCudoviste.setHealth(healthMete);
        Logger.getInstance().log("[Carobnjak] je napao [" + napadnutoCudoviste + "] pomocu [" + this.trenutnoOruzije + "]");
    }

    @Override
    public void napadni() {

    }

    @Override
    public void uzmiOruzije(Oruzije oruzije) {
        try {
            switch (oruzije.getTip()) {
                case KOPLJE -> throw new Exception("Carobnjak ne moze da koristi koplje!");
                case MAC -> throw new Exception("Carobnjak ne moze da koristi mac!");
                case CAROLIJA -> {
                    ranac.dodajOruzije(oruzije);
                    Logger.getInstance().log("[Carobnjak] je pokupio oruzije [" + oruzije + "]");
                }
            }
        } catch (Exception e) {
            System.out.println("Exception: " + e);
        }
    }

}
