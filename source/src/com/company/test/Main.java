package com.company.test;

public class Main {

    public static void borba(Heroj heroj, Cudoviste cudoviste) {
        boolean borbaUToku = true;
        while (borbaUToku) {

            int x = (int)(Math.random() * ((100 - 0) + 1) + 0);

            if ( x < 50) {
                heroj.napadni(cudoviste);
            } else if ( x >= 50) {
                cudoviste.napadni(heroj);
            }

            if (heroj.getHealth() <= 0) {
                Logger.getInstance().log("[" + cudoviste + "]  je pobedio u duelu sa [" + heroj +"]");
                borbaUToku = false;
            } else if (cudoviste.getHealth() <= 0) {
                Logger.getInstance().log("[" + heroj + "]  je pobedio u duelu sa [" + cudoviste +"]");
                borbaUToku = false;
            }
        }
    }

    public static void main(String[] args) {
	// write your code here
        System.out.println("Pozdrav");

        Oruzije mac1 = new Oruzije(TipOruzija.MAC);
        Oruzije mac2 = new Oruzije(TipOruzija.MAC);
        Oruzije koplje = new Oruzije(TipOruzija.KOPLJE);
        Oruzije carolija = new Oruzije(TipOruzija.CAROLIJA);

        Cudoviste pauk = new Pauk();
        Cudoviste zmaj = new Zmaj();

        Ranac ranac1 = new Ranac();
        Ranac ranac2 = new Ranac();

        Heroj macevalac = new Macevalac(ranac1);
        Heroj carobnjak = new Carobnjak(ranac2);

        macevalac.uzmiOruzije(carolija);
        System.out.println(macevalac.trenutnoOruzije);

        carobnjak.uzmiOruzije(mac2);
        System.out.println(carobnjak.trenutnoOruzije);

        carobnjak.uzmiOruzije(carolija);
        carobnjak.promeniTrenutnoOruzije(0);
        System.out.println(carobnjak.trenutnoOruzije);

        macevalac.baciOruzije(0);
        macevalac.uzmiOruzije(mac2);
        macevalac.promeniTrenutnoOruzije(0);
        System.out.println(macevalac.trenutnoOruzije);
        macevalac.baciOruzije(0);
        System.out.println(macevalac.trenutnoOruzije);

        macevalac.uzmiOruzije(mac2);
        macevalac.uzmiOruzije(mac2);

        macevalac.uzmiOruzije(mac1);
        macevalac.uzmiOruzije(koplje);
        macevalac.promeniTrenutnoOruzije(0);
        System.out.println(macevalac.trenutnoOruzije);

        borba(macevalac, pauk);
        borba(carobnjak, zmaj);

    }

}
