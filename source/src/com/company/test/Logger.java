package com.company.test;

public class Logger {

    private static Logger INSTANCE;

    // Nisam stigao da implementiram da logger upisuje u fajl stvari pa sam stavio da ih samo printuje. :)
    public void log(String messege) {
        System.out.println(messege);
    }

    private Logger() {
        if (INSTANCE != null) {
            throw new IllegalStateException("Logger already instantiated");
        }
    }

    public static Logger getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new Logger();
        }
        return INSTANCE;
    }
}
