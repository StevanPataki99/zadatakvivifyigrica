package com.company.test;

public class Zmaj extends Cudoviste{

    public Zmaj() {
        this.health = 80;
    }

    @Override
    public void napadni(Heroj napadnutiHeroj) {
        int x = (int)(Math.random() * ((100 - 0) + 1) + 0);
        if (x < 50) {
            this.udar(napadnutiHeroj);
            Logger.getInstance().log("[Zmaj] je napao [" + napadnutiHeroj + "] pomocu [udar]");
        } else if (x >= 50) {
            this.bljujeVatru(napadnutiHeroj);
            Logger.getInstance().log("[Zmaj] je napao [" + napadnutiHeroj + "] pomocu [bljujeVatru]");
        }
    }

    private void udar(Heroj napadnutiHeroj) {
        int healthMete = napadnutiHeroj.getHealth();
        healthMete = healthMete - 5;
        napadnutiHeroj.setHealth(healthMete);
    }

    private void bljujeVatru(Heroj napadnutiHeroj) {
        int healthMete = napadnutiHeroj.getHealth();
        healthMete = healthMete - 20;
        napadnutiHeroj.setHealth(healthMete);
    }

    @Override
    public void napadni() {

    }
}
