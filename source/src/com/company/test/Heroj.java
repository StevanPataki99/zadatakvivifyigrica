package com.company.test;

public abstract class Heroj implements Bice {

    protected int health;
    protected Ranac ranac;
    protected Oruzije trenutnoOruzije;

    public Heroj(Ranac ranac) {
        this.ranac = ranac;
    }

    public abstract void napadni(Cudoviste napadnutoCudoviste);

    public abstract void uzmiOruzije(Oruzije oruzije);

    public void baciOruzije(int indexOruzija) {
        ranac.baciOruzije(indexOruzija);
        this.trenutnoOruzije = ranac.koristiOruzije(ranac.getTrenutanBrojOruzija() - 1);
    }

    public void promeniTrenutnoOruzije(int indexOruzija) {
        this.trenutnoOruzije = ranac.koristiOruzije(indexOruzija);
    }

    public Ranac getRanac() {
        return this.ranac;
    }

    public void setRanac(Ranac noviRanac) {
        this.ranac = noviRanac;
    }

    public int getHealth() {
        return this.health;
    }

    public void setHealth(int noviHealth) {
        this.health = noviHealth;
    }

    public Oruzije getOruzije() {
        return trenutnoOruzije;
    }

}
