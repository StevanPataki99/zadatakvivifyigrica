package com.company.test;

import java.util.ArrayList;

public class Ranac {
    private final int maksimalanBrojOruzija = 2;
    private ArrayList<Oruzije> oruzijaURancu = new ArrayList<Oruzije>();

    public Ranac() {}

    public void dodajOruzije(Oruzije oruzije) {
        try {
            if ( oruzijaURancu.size() >= 2 ) {
                throw new Exception("Ranac je pun");
            } else if(oruzije.getBaceno()) {
                oruzije.setBaceno(false);
                oruzijaURancu.add(oruzije);
            } else {
                throw new Exception("Oruzije nije na zemlji");
            }
        } catch (Exception e) {
            System.out.println("Exception: " + e);
        }
    }

    public Oruzije koristiOruzije(int indexOruzija) {
        try {
            if ( oruzijaURancu.size() != 0) {
                return oruzijaURancu.get(indexOruzija);
            } else {
                throw new Exception("NoWeapon (Nemas ni jedno oruzije u rancu)");
            }
        } catch (Exception e) {
            System.out.println("Exception: " + e);
        }
        return null;
    }

    public void baciOruzije(int indexOruzija) {
        try {
            if ( oruzijaURancu.size() != 0) {
                Oruzije bacenoOruzije = oruzijaURancu.get(indexOruzija);
                bacenoOruzije.setBaceno(true);
                oruzijaURancu.remove(indexOruzija);
            } else {
                throw new Exception("NoWeapon  (Nemas sta da bacis)");
            }
        } catch (Exception e) {
            System.out.println("Exception: " + e);
        }
    }

    public int getTrenutanBrojOruzija() {
        return oruzijaURancu.size();
    }

}
