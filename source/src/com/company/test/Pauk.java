package com.company.test;

public class Pauk extends Cudoviste {

    public Pauk() {
        this.health = 30;
    }

    @Override
    public void napadni(Heroj napadnutiHeroj) {
        int x = (int)(Math.random() * ((100 - 0) + 1) + 0);
        if (x < 50) {
            this.udar(napadnutiHeroj);
            Logger.getInstance().log("[Pauk] je napao [" + napadnutiHeroj + "] pomocu [udar]");
        } else if (x >= 50) {
            this.ujed(napadnutiHeroj);
            Logger.getInstance().log("[Pauk] je napao [" + napadnutiHeroj + "] pomocu [ujed]");
        }
    }

    private void udar(Heroj napadnutiHeroj) {
        int healthMete = napadnutiHeroj.getHealth();
        healthMete = healthMete - 5;
        napadnutiHeroj.setHealth(healthMete);
    }

    private void ujed(Heroj napadnutiHeroj) {
        int healthMete = napadnutiHeroj.getHealth();
        healthMete = healthMete - 8;
        napadnutiHeroj.setHealth(healthMete);
    }

    @Override
    public void napadni() {

    }
}
